#!/usr/bin/env bash

mkdir --parent target

cp '../1. Install Infrastructure - Terraform/target/state.json' src/terraform

export epsilonInstall=/home/jg/eclipse/epsilon-Interim2

export launcher=${epsilonInstall}/eclipse/plugins/org.eclipse.equinox.launcher_*.jar

java -jar ${launcher} -application org.eclipse.ant.core.antRunner -buildfile "src/ant/convert_terraform_state_to_ansible_inventory.ant"
